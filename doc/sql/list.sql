### https://www.jfinal.com/share/1655
### @久伴轻尘
#sql("list")
SELECT
	`admin_dictionary`.*
	#if(join)
		,`admin_dictionary_type`.codeItemName
	#end
FROM
	admin_dictionary AS `admin_dictionary`
	#if(join)
		INNER JOIN admin_dictionary_type AS `admin_dictionary_type`
		ON `admin_dictionary`.codeItemId=`admin_dictionary_type`.codeItemId
	#end
WHERE
	#@dictionaryWheres()
#end

#define dictionaryWheres()
	1=1
	#if(id)
		AND `admin_dictionary`.dictionaryId=#para(id)
	#end

	#if(dictionaryId)
		AND `admin_dictionary`.dictionaryId LIKE concat('%',#para(dictionaryId),'%')
  #end

  #if(codeItemId)
    AND `admin_dictionary`.codeItemId=#para(codeItemId)
  #end