package com.jxtpro.sutra.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.upload.UploadFile;
import com.jxtpro.sutra.comment.utils.ResultData;
import com.xiaoleilu.hutool.collection.CollUtil;
import com.xiaoleilu.hutool.poi.excel.ExcelUtil;
import com.xiaoleilu.hutool.poi.excel.ExcelWriter;

/**
 * 各种功能
 * 
 * 	1.导入导出excel
 *  2.上传图片,下载文件
 *  3.select 选择 
 *  4.select 二级联动
 *  5.百度编辑器的简单使用
 *  6.全选/全不选
 *  7.ztree的使用
 *  
 * 	@author x
 */
public class DemoController extends Controller{
	
	public static final String[] imgexts = {"png","jpg"};
	
	public void index() {
		render("index.html");
	}
	
	/**
	 * 1. --------------------------------------------- 导入导出excel ----------------------------------
	 */
	public void importExcel() {
		renderJson(doUpload(getFiles("file")));
	}
	public ResultData doUpload(List<UploadFile> files) {
		//是否有空文件
		if(haveUploadFileEmpty(files)) {
			//TODO 默认 不处理 不继续
			return new ResultData().setFaild(1,"error", "批量文件中含有空文件");
		}
		for(UploadFile item : files) {
			//TODO 业务
			doBizIntoDb(item);
		}
		return new ResultData().setSuccess(0, "success", "http://cdn.layui.com/123.jpg");
	}
	private void doBizIntoDb(UploadFile item) {
		System.out.println(item.getFileName());
	}
	//有空文件
	private boolean haveUploadFileEmpty(List<UploadFile> files) {
		for(UploadFile item : files) {
			if(isUploadFileEmpty(item)) {
				return true;
			}
		}
		return false;
	}
	//是空文件
	private boolean isUploadFileEmpty(UploadFile item) {
		if(item.getFile() != null && item.getFile().length() > 0) {
			return false;
		}
		return true;
	}
	
	public void exportExcel() {
		ExcelWriter writer = ExcelUtil.getWriter();
		List<String> headRow =CollUtil.newArrayList("指数名称", "指数值", "指数日期", "涨跌*", "涨跌幅", "开盘", "最高", "最低", "昨收", "昨收涨跌", "昨收涨跌率", "备注");
		//TODO  填充数据
		List<List<String>> rows = new ArrayList<>();
		
		// 设置这些样式
		writer.getHeadCellStyle().setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
		writer.getHeadCellStyle().setFillPattern(FillPatternType.SOLID_FOREGROUND);
		writer.getHeadCellStyle().setBorderBottom(BorderStyle.THIN);
		writer.getHeadCellStyle().setBorderLeft(BorderStyle.THIN);
		writer.getHeadCellStyle().setBorderRight(BorderStyle.THIN);
		writer.getHeadCellStyle().setBorderTop(BorderStyle.THIN);
		writer.getHeadCellStyle().setAlignment(HorizontalAlignment.CENTER);
		// 生成一个字体
		Font font = writer.getWorkbook().createFont();
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		writer.writeHeadRow(headRow);
		writer.write(rows);
		
		ServletOutputStream os = null;
		try {
		getResponse().setContentType("application/x-msdownload");
		// inline在浏览器中直接显示，不提示用户下载
		// attachment弹出对话框，提示用户进行下载保存本地
		// 默认为inline方式
		getResponse().setHeader("Content-Disposition", "attachment;filename="
					+ new String("测试".getBytes(),"iso-8859-1") + ".xls");
		getResponse().setCharacterEncoding("utf-8");
		os = getResponse().getOutputStream();
			writer.flush(os);
		} catch (IOException e) {
		}finally{
			try {
				if(os != null) {
					os.close();
				}
				if(writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				
			}
			
		}
	}
	
	/**
	 * 2. --------------------------------------------- 上传图片,下载文件 ----------------------------------
	 */
	public void uploadImg(){
		renderJson(doUpload(getFile()));
	}
	/**
	 * 可做判断 图片大小/ext/...
	 */
	public ResultData doUpload(UploadFile uf){
		ResultData result = new ResultData();
		if(uf == null ){
			return result.setFaild(111, "请选择图片", null);
		}
		String fileName = uf.getFile().getName(); //先简单判断 ext 
		if(!isInExts(fileName)){
			return result.setFaild(111, "支持[png,jpg]图片", null);
		}
		//小于等于 200 k 图片
		if(uf.getFile().length() > 1024 * 200){ 
			return result.setFaild(111, "请选择图片", null);
		}
		//TODO 可新增param 做其他业务操作 如连接 阿里云服务器 oss /七牛云 ] 存本地数据库记录数据
		return result.setSuccess(0, "上传成功", null); // code == 0 表示上传成功
	}
	/**
	 * 简单验证是否符合图片扩展名称
	 */
	public boolean isInExts(String fileName){
		for (int i = 0; i < imgexts.length; i++) {
			if(fileName.contains(imgexts[i]) && fileName.charAt(fileName.lastIndexOf(imgexts[i]) -1) == '.'){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * 3. --------------------------------------------- select 选择   ----------------------------------
	 * kv形式
	 */
	public void loadSelectData() {
		String sql = "select from";
		renderJson(Db.find(sql));
	}
	
	/**
	 * 4. --------------------------------------------- select 二级联动  ----------------------------------
	 */
	public void loadSelectChildData() {
		String sql = "select from" + getParaToInt("id",0);
		renderJson(Db.find(sql));
	}
	
	/**
	 * 5. --------------------------------------------- 百度编辑器的简单使用  ----------------------------------
	 */

	/**
	 * 6. --------------------------------------------- 全选/全不选  ----------------------------------
	 */
	
	/**
	 * 7. --------------------------------------------- 7.ztree的使用  ----------------------------------
	 */
}
