package com.cyy.app

import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val tackyButton by cssclass()
        val heading by cssclass()
        val txt100 by cssclass()
        private val topColor = c("#FF0000")
        private val rightColor = c("#006400")
        private val bottomColor = c("#FFA500")
        private val leftColor = c("#800080")
    }

    init {
        label and heading {
//            padding = box(10.px)
            fontSize = 15.px
            fontWeight = FontWeight.BOLD
        }
        tackyButton {
//            rotate = 10.deg
            borderColor += box(topColor,rightColor,bottomColor,leftColor)
            fontFamily = "Comic Sans MS"
            fontSize = 20.px
        }
        txt100{
            prefWidth=100.px
        }
    }
}