package com.cyy.view

import cn.hutool.core.io.FileUtil
import com.cyy.controller.CyyGenerator
import com.cyy.model.Constants
import com.cyy.model.GmodelModel
import com.cyy.srv.GenSrv
import com.cyy.view.generator.GenCenter
import com.cyy.view.generator.GenLeft
import com.jfinal.kit.Kv
import com.jfinal.kit.StrKit
import com.jfinal.plugin.activerecord.Db
import com.jfinal.plugin.activerecord.generator.TypeMapping
import javafx.geometry.Orientation
import javafx.scene.control.RadioButton
import javafx.scene.control.SelectionMode
import javafx.scene.control.TreeItem
import javafx.stage.FileChooser
import tornadofx.*
import tornadofx.controlsfx.breadcrumbbar
import tornadofx.controlsfx.treeitem
import java.io.File

class DBView() : View() {
    val gmodel: GmodelModel by inject()

    override val root = borderpane() {
        left {
            add(GenLeft())
        }
        center {
            add(GenCenter())
        }
    }

}



