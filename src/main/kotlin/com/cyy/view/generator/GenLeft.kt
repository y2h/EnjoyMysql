package com.cyy.view.generator

import cn.hutool.core.io.FileUtil
import com.cyy.model.GmodelModel
import javafx.geometry.Orientation
import javafx.stage.FileChooser
import tornadofx.*
import java.io.File

class GenLeft : View("My View") {
    val gmodel:GmodelModel by inject()
    override val root = vbox {
        titledpane("目录设置") {
            vbox {
                form() {
                    fieldset("生成项目目录", labelPosition = Orientation.VERTICAL) {
                        field("模板文件根目录") {
                            button("...") {
                                action {
                                    try {
                                        val fn = chooseDirectory("模板文件根目录", FileUtil.getUserHomeDir()) {
                                            // p初始目录为当前项目目录
                                            initialDirectory = File(File("").canonicalPath)
                                        }
                                        if (fn != null) {
                                            gmodel.baseTemplatePath.value = fn.absolutePath

                                        }
                                    } catch (e: Exception) {
                                        println(e.toString())
                                        return@action
                                    }
                                }
                            }
                            textfield(gmodel.baseTemplatePath) {
                                text = "generator"
                                required(message = "Enter user 模板文件根目录")
                            }
                        }

                        field("选择配置文件") {
                            button("...") {
                                action {
                                    val efset = arrayOf(FileChooser.ExtensionFilter("选择配置文件", "*.*"))

                                    val fnset = chooseFile("选择配置文件", efset, FileChooserMode.Single) {
                                        // p初始目录为当前项目目录
                                        initialDirectory = File(File("").canonicalPath)
                                    }
                                    if (fnset.isNotEmpty()) {
                                        gmodel.settingFile.value = "${fnset.first()}"
                                    }
                                }
                            }
                            textfield(gmodel.settingFile) {
                                text = "generator/generator.properties"
                            }
                        }
                        field("projectName") {
                            textfield(gmodel.projectName) {
                                text = "jxtpro"
                            }
                        }
                        field("baseSrcPath") {
                            textfield(gmodel.baseSrcPath) {
                                text = "src/main/java/"
                            }
                        }
                        field("baseWebAppPath") {
                            textfield(gmodel.baseWebAppPath) {
                                text = "src/main/webapp/"
                            }
                        }
                        field("baseResourcesPath") {
                            textfield(gmodel.baseResourcesPath) {
                                text = "src/main/resources/"
                            }
                        }
                        field("baseWEBINFPath") {
                            textfield(gmodel.baseWEBINFPath) {
                                text = "src/main/webapp/WEB-INF/"
                            }
                        }

                        field("projectPackage") {
                            textfield(gmodel.projectPackage) {
                                text = "com.jxtpro.demo"
                            }
                        }
                    }
                }
            }
        }
    }
}
