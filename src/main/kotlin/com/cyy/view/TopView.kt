package com.cyy.view

import cn.hutool.core.io.FileUtil
import cn.hutool.core.util.CharsetUtil
import cn.hutool.core.util.ReflectUtil
import cn.hutool.setting.Setting
import com.cyy.app.Styles
import com.cyy.controller.CyyGenerator
import com.cyy.model.Constants
import com.cyy.model.GmodelModel
import com.cyy.srv.GenSrv
import com.jfinal.kit.Kv
import com.jfinal.kit.StrKit
import com.jfinal.plugin.activerecord.Config
import com.jfinal.plugin.activerecord.Db
import com.jfinal.plugin.activerecord.DbKit
import com.jfinal.plugin.activerecord.DbPro
import com.jfinal.plugin.activerecord.dialect.Dialect
import com.jfinal.plugin.activerecord.generator.TypeMapping
import com.jfinal.template.EngineConfig
import javafx.geometry.Orientation
import javafx.scene.control.ListView
import javafx.scene.control.TextFormatter
import javafx.stage.FileChooser
import tornadofx.*
import java.io.File
import java.util.*
import javax.sql.DataSource

class TopView(val gmodel: GmodelModel, val genSrv: GenSrv) : View("My View") {
    val engine = gmodel.engine.value
    var tbls = gmodel.tables
    lateinit var gen: CyyGenerator
    val newDb = stringProperty()
    // 限定textfield输入值
    val FirstTenFilter: (TextFormatter.Change) -> Boolean = { change ->
        !change.isAdded || change.controlNewText.let {
            it.isInt() && it.toInt() in 0..65535
        }
    }

    override val root = titledpane("数据库设置") {
        hbox(5) {
            form {
                fieldset(labelPosition = Orientation.VERTICAL) {
                    hbox(10) {
                        field("主机地址") {
                            textfield(gmodel.host) {
                                addClass(Styles.txt100)
                                text = "localhost"
                                required(message = "Enter host for your database")
                            }
                        }
                        field("主机端口") {
                            textfield(gmodel.port) {
                                addClass(Styles.txt100)
                                text = "3306"
                                filterInput(FirstTenFilter)
                                required(message = "Enter port for your database")
                            }
                        }
                        field("用户名") {
                            textfield(gmodel.user) {
                                addClass(Styles.txt100)
                                text = "root"
                                required(message = "Enter user name for your database")
                            }
                        }
                        field("密码") {
                            textfield(gmodel.pwd) {
                                addClass(Styles.txt100)
                                text = "root"
                                required(message = "Enter user name for your database")
                            }
                        }
                        field {
                            vbox(10) {
                                button("测试连接") {
                                    action {
                                        if ((gmodel.arp.value != null)) {
//                    Constants.closeDb(gmodel.dataSource.value)
                                            gmodel.arp.value.stop()
                                        }
                                        // 每次点击"测试连接"测试连接时，都将之前保存的db和table列表清空
                                        gmodel.dbs.clear()
                                        gmodel.tables.clear()
                                        if (testDb2().not()) return@action
                                        gmodel.dbOK.value = true
                                    }
                                }
                                button("创建数据库") {
                                    enableWhen(gmodel.dbOK)
                                    action {
                                       if(genSrv.createDb(newDb.value)) {
                                           gmodel.dbs.add(newDb.value)
                                       }else{
                                           fire(GenEvent("创建数据库share1655失败！"))
                                       }
                                    }
                                }

                                textfield(newDb) {
                                    text = "share1655"
                                }

                            }

                        }

                        field("数据库") {
                            combobox(gmodel.dbname, gmodel.dbs) {
                                selectionModel.selectedItemProperty().addListener { _, _, _ ->
                                    (if (!selectionModel.selectedItem.isNullOrEmpty()) {
                                        run{
                                            genSrv.prapare()
                                            gmodel.tableCounts.value = "当前数据库中共有${gmodel.tables.size.toString()}张表"
                                        }
                                    })
                                }
                            }
                        }
                        field("数据表") {
                            combobox(gmodel.leftTable, gmodel.tables)
                            {
                                selectionModel.selectedItemProperty().addListener { _, _, _ ->
                                    (if (!selectionModel.selectedItem.isNullOrEmpty()) {

                                            gmodel.columns.clear()
                                            gmodel.getColumnsSql.value = "select column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='${gmodel.dbname.value}' and TABLE_NAME='${gmodel.leftTable.value}'"
                                            gmodel.columns.addAll(genSrv.getList(gmodel.dataSource.value.connection, gmodel.getColumnsSql.value))
//
                                    })
                                }
                            }
                        }
                        field("表字段") {
                            combobox(gmodel.column, gmodel.columns)
                        }
                        field("查寻") {
                            combobox(gmodel.column, gmodel.columns)
                        }
                    }

                    hbox(10) {
                        field("jdbcUrl") {
                            textfield(gmodel.jdbcUrl) {
                                //                            text = "jdbc:sqlite://soft/pboot.db"
                                required(message = "Enter user name for your database")
                            }
                        }
                        field("选择模板文件") {
                            val tmpl= stringProperty()
                            val projectPath=gmodel.curProjectPath.value
                            button("...") {
                                action {
                                    val efset = arrayOf(FileChooser.ExtensionFilter("选择模板文件", "*.*"))

                                    val fnset = chooseFile("选择模板文件", efset, FileChooserMode.Single) {
                                        // p初始目录为当前项目目录
                                        initialDirectory = File(projectPath)
                                    }
                                    if (fnset.isNotEmpty()) {
                                        tmpl.value = "${fnset.first()}"
                                    }
                                }
                            }
                            textfield(tmpl) {
                                text = "${projectPath}/doc/tmpl/gui/crud.txt"
                            }

                            button("一键生成CRUD-GUI"){
                                action{
                                    var outPath="""${FileUtil.getUserHomePath()}/CRUD-GUI/src/main/kotlin/"""
                                    gmodel.metaBuilder.value=genSrv.prepareMetaBuilder()
                                    val tableMetas=gmodel.metaBuilder.value.build()
                                    val content=Kv()
                                    run {
                                        for (table in tableMetas) {
                                            val claLowName = StrKit.firstCharToLowerCase(StrKit.toCamelCase(table.modelName))
                                            val claUpName = StrKit.firstCharToUpperCase(StrKit.toCamelCase(table.modelName))
                                            content["remarks"] = table.remarks
                                            content["cols"] = table.columnMetas
                                            content["claUpName"] = claUpName
                                            content["claLowName"] = claLowName
                                            content["mainClass"] = "CRUD_GUI"
                                            Constants.writeFiles(engine.getTemplate(tmpl.value), content, "${outPath}${claUpName}.kt", true)
                                        }
                                        val gradleFilePath = File(tmpl.value).parent
                                        outPath = """${FileUtil.getUserHomePath()}/CRUD-GUI/build.gradle"""
                                        Constants.writeFiles(engine.getTemplate("${gradleFilePath}/build.gradle"), content, outPath, true)
                                        // 生成main app
                                        outPath = """${FileUtil.getUserHomePath()}/CRUD-GUI/src/main/kotlin/CRUD_GUI.kt"""
                                        val t=tableMetas.map{
                                            StrKit.firstCharToUpperCase(StrKit.toCamelCase(it.modelName))
                                        }
                                        val t1=tableMetas.map{
                                            StrKit.firstCharToLowerCase(StrKit.toCamelCase(it.modelName))
                                        }
                                        content["views"] = t
                                        content["claLowName"] = t1
                                        val appPath = File(tmpl.value).parent

                                        Constants.writeFiles(engine.getTemplate("${appPath}/CRUD_GUI.kt.txt"), content, outPath, true)
                                        println("------------ ok 刷新项目  --------")
                                    }
                                }
                            }
                        }

                    }
                }
            }
//        label("HOST:")
//        textfield(gmodel.host) {
//            addClass(Styles.txt100)
//            text = "localhost"
//            required(message = "Enter host for your database")
//        }
//        label("PORT:")
//        textfield(gmodel.port) {
//            addClass(Styles.txt100)
//            text = "3306"
//            filterInput(FirstTenFilter)
//            required(message = "Enter port for your database")
//        }
//        label("USER:")
//        textfield(gmodel.user) {
//            addClass(Styles.txt100)
//            text = "root"
//            required(message = "Enter user name for your database")
//        }
//        label("PWD:")
//        textfield(gmodel.pwd) {
//            addClass(Styles.txt100)
//            text = "root"
//            required(message = "Enter user name for your database")
//        }
//        button("测试连接") {
//            action {
//                if ((gmodel.arp.value!= null)) {
////                    Constants.closeDb(gmodel.dataSource.value)
//                    gmodel.arp.value.stop()
//                }
//                // 每次点击"测试连接"测试连接时，都将之前保存的db和table列表清空
//                gmodel.dbs.clear()
//                gmodel.tables.clear()
//                if (testDb2().not()) return@action
//            }
//        }
//        combobox(gmodel.dbname, gmodel.dbs){
//            selectionModel.selectedItemProperty().addListener { _, _, _ ->
//                (if (!selectionModel.selectedItem.isNullOrEmpty()) {
//                    genSrv.prapare()
//                })
//            }
//        }
//        combobox(gmodel.leftTable, gmodel.tables)


        }
    }


    /**
     * test connection for postgresql, mysql,oracle,sqlserver
     */
    fun testDb2(): Boolean {
        try {
            genSrv.getCon()
            gmodel.dbOK.value = true
            genSrv.getDBs()
            fire(GenEvent("恭喜！数据库连接成功！"))
            return true
        } catch (e: Exception) {
            gmodel.dbOK.value = false
            fire(GenEvent("testDb() test connection failed: $e"))
            return false
        }
    }
}

