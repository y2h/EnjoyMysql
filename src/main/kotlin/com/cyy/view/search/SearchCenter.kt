package com.cyy.view.search


import com.cyy.model.GmodelModel
import tornadofx.*


class SearchCenter : View("My View") {
    override val root = borderpane {
        center {
            add(WbView())
        }
    }
}

class WbView:View(){
    val gm: GmodelModel by inject()

    override val root=webview {
        dynamicContent(gm.tplStringOut) {
            run{
                this.engine.loadContent(gm.tplStringOut.value)
            }
        }
    }
}

