package com.cyy.view.search


import com.cyy.model.GmodelModel
import tornadofx.*

class Search(val gm: GmodelModel) : View("My View") {
    override val root = scrollpane(true, true){
        borderpane {
            paddingAll=10
            left {
                add(SearchLeft())
            }
            center {
                add(SearchCenter())

            }
        }
    }

}


